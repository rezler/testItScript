from django.shortcuts import render_to_response
from datetime import datetime

from helpers.statistics import Statistic
from helpers.uploads import write_logs
from testItScript.forms import DocumentForm
from .models import Document


def upload(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        newdoc = Document(docfile=request.FILES['docfile'])
        newdoc.save()
        write_logs(newdoc)
    else:
        form = DocumentForm()

    return render_to_response(
        'index.html',
        {'form': form},
    )


def get_statistic(request):
    start_date = request.POST['start_date']
    stop_date = request.POST['stop_date']

    start_date = datetime.strptime(start_date, '%Y-%m-%d')
    stop_date = datetime.strptime(stop_date, '%Y-%m-%d')

    h = Statistic(start_date=start_date, stop_date=stop_date)
    bike_rental_time = h.fetch()

    return render_to_response(
        'statistic.html',
        {'data': bike_rental_time}
    )
