from django.db import models


class EventLog(models.Model):
    ARRIVED = 1
    DEPARTED = 2

    EVENT = (
        (ARRIVED, 'was docked'),
        (DEPARTED, 'was rented out'),
    )

    event = models.IntegerField(choices=EVENT)
    moment = models.DateTimeField()
    bike = models.IntegerField()
    station = models.IntegerField()


class Document(models.Model):
    docfile = models.FileField(upload_to='documents/')
