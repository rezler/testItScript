
from django.contrib import admin
from django.urls import path
from django.conf.urls import url

from logs.views import get_statistic, upload

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'/get_statistic/', get_statistic, name='get_statistic'),
    url(r'^$', upload, name='upload'),
]
