from datetime import timedelta
import time
from itertools import groupby
from collections import namedtuple

from logs.models import EventLog


class Statistic:

    def __init__(self, start_date, stop_date):
        self.start_date = start_date
        self.stop_date = stop_date + timedelta(days=1)
        self.bike_rental_time = []
        self.total = timedelta()

    def fetch(self):
        self.count_bike_rental_time()
        return self.bike_rental_time

    def count_bike_rental_time(self):
        for data in self.build_rental_periods():
            self.bike_rental_time.append({
                'bike': data['bike'],
                'time': self.calculate_time(data['rental_periods'])
            })
            self.bike_rental_time.append({
                'bike': 'all',
                'time': self.format_deltatime(self.total)
            })

    def build_rental_periods(self):
        bike_rental_periods = []
        grouped_logs = groupby(self._log_qs(), key=lambda log: log.bike)

        for bike, logs in grouped_logs:
            rental_periods = self.build_rental_periods_for_bike(logs=logs)
            bike_rental_periods.append({
                'bike': bike,
                'rental_periods': rental_periods
            })
        return bike_rental_periods

    def _log_qs(self):
        return EventLog.objects.filter(
            moment__range=(self.start_date, self.stop_date)
        )

    @staticmethod
    def build_rental_periods_for_bike(logs):
        rental_periods = []
        pr = RentalPeriod()

        for log in logs:
            if log.event == EventLog.DEPARTED:
                pr.departed_date = log.moment

            elif log.event == EventLog.ARRIVED:
                pr.arrived_date = log.moment
                rental_periods.append(pr)
                pr = RentalPeriod()

        return rental_periods

    def calculate_time(self, rental_periods):
        delta = timedelta()
        print(rental_periods)
        for period in rental_periods:
            if not period.arrived_date:
                period.arrived_date = self.stop_date + timedelta(days=1)
            if not period.departed_date:
                period.departed_date = self.start_date
                print(period.arrived_date, '---', period.departed_date)
            delta += (period.arrived_date - period.departed_date)
            self.total += (period.arrived_date - period.departed_date)

        return self.format_deltatime(delta)

    @staticmethod
    def format_deltatime(delta):
        if not delta:
            return ''
        rental_time = time.strftime("%H:%M:%S", time.gmtime(delta.seconds))
        hour = rental_time[:2]
        return rental_time.replace(hour, str(int(hour) + delta.days*24))


class RentalPeriod:
    def __init__(self):
        self.departed_date = None
        self.arrived_date = None
