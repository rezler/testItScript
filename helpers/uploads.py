from datetime import datetime

from logs.models import EventLog
from testItScript.settings import MEDIA_ROOT


def write_logs(file):
    path = MEDIA_ROOT + '/' + str(file.docfile)
    events = []
    if not file:
        return
    with open(path, 'r') as f:
        for line in f:
            # "22,102,20150304T13:04:00,20150304T13:25:32"
            line = line.replace('"', '').split(',')
            line = [elem.rstrip() for elem in line]

            if line[2]:
                moment = datetime.strptime(line[2], '%Y%d%mT%H:%M:%S')
                events.append(EventLog(
                    bike=line[1],
                    station=line[0],
                    moment=moment,
                    event=EventLog.ARRIVED
                ))
            if line[3]:
                moment = datetime.strptime(line[3], '%Y%d%mT%H:%M:%S')
                events.append(EventLog(
                    bike=line[1],
                    station=line[0],
                    moment=moment,
                    event=EventLog.DEPARTED
                ))

    EventLog.objects.bulk_create(events)
